package com.j2core.ice.week04.zooemulation.animals;
/**
 * Created by Serge Kotvitsky on 9/6/16.
 */
public enum AnimalType {
    PREDATOR,HERBIVOROUS,OMNIVOROUS
}
