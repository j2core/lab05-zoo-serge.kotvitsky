package com.j2core.ice.week04.zooemulation.animals;
import java.util.List;
import java.util.Random;
/**
 * Created by Serge Kotvitsky on 8/20/16.
 */
public class Squirrel extends Animal {

    public Squirrel(String alias, AnimalType animalType, int dangerLevel, int satietyLevel, int vivacityLevel,
                    int vitality, int minFoodEaten, int minSleepTime) {
        super(alias, animalType, dangerLevel, satietyLevel, vivacityLevel, vitality, minFoodEaten, minSleepTime);
    }
    @Override
    public int animate(int food, List<Animal> animals) {

        if (getVivacityLevel() < new Random().nextInt(10) + 40)
            sleep();
        else if (getSatietyLevel() < new Random().nextInt(10) + 60)
            food = eat(food);
        else move();
        return food;
    }
}
