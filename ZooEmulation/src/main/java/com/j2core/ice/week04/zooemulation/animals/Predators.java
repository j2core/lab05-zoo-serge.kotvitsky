package com.j2core.ice.week04.zooemulation.animals;
import java.util.List;
/**
 * Created by Serge Kotvitsky on 9/27/16.
 */
public interface Predators {
    /**
     * Method describes the process of cannibalism herbivores
     * @param animals The list of victims herbivores
     */
    void eatingHerbivorousAnimal(List<Animal> animals);
}
