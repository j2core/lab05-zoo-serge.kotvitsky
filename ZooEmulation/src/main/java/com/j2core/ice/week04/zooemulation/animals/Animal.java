package com.j2core.ice.week04.zooemulation.animals;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.util.List;
import java.util.Random;
/**
 * Created by Serge Kotvitsky on 8/20/16.
 */
public abstract class Animal {

    private boolean alive = true;
    private String alias;
    private AnimalType animalType;
    private int dangerLevel;
    private int satietyLevel;
    private int vivacityLevel;
    private int vitality;
    private int minFoodEaten;
    private int minSleepTime;
    private int maxFoodEaten;
    private int maxSleepTime;
    public static final int MIN_PERCENT = 0;
    public static final int MAX_PERCENT = 100;
    Logger logger = LogManager.getLogger(Animal.class);

    public Animal(String alias, AnimalType animalType, int dangerLevel, int satietyLevel, int vivacityLevel, int vitality,
                  int minFoodEaten, int minSleepTime) {
        this.alias = alias;
        this.animalType = animalType;
        this.dangerLevel = dangerLevel;
        this.satietyLevel = satietyLevel;
        this.vivacityLevel = vivacityLevel;
        this.vitality = vitality;
        this.minFoodEaten = minFoodEaten;
        this.minSleepTime = minSleepTime;
        this.maxFoodEaten = minFoodEaten + minFoodEaten;
        this.maxSleepTime = minSleepTime + minSleepTime;
    }
    public String getAlias() {
        return alias;
    }
    public void setAlias(String alias) {
        this.alias = alias;
    }
    public boolean isAlive() {
        return alive;
    }
    public void setAlive(boolean alive) {
        this.alive = alive;
    }
    public AnimalType getAnimalType() {
        return animalType;
    }
    public void setAnimalType(AnimalType animalType) {
        this.animalType = animalType;
    }
    public int getDangerLevel() {
        return dangerLevel;
    }
    public void setDangerLevel(int dangerLevel) {
        this.dangerLevel = dangerLevel;
    }
    public int getSatietyLevel() {
        return satietyLevel;
    }
    public void setSatietyLevel(int satietyLevel) {
        this.satietyLevel = satietyLevel;
    }
    public int getVivacityLevel() {
        return vivacityLevel;
    }
    public void setVivacityLevel(int vivacityLevel) {
        this.vivacityLevel = vivacityLevel;
    }
    public int getVitality() {
        return vitality;
    }
    public void setVitality(int vitality) {
        this.vitality = vitality;
    }
    public int getMinFoodEaten() {
        return minFoodEaten;
    }
    public void setMinFoodEaten(int minFoodEaten) {
        this.minFoodEaten = minFoodEaten;
    }
    public int getMinSleepTime() {
        return minSleepTime;
    }
    public void setMinSleepTime(int minSleepTime) {
        this.minSleepTime = minSleepTime;
    }
    public int getMaxFoodEaten() {
        return maxFoodEaten;
    }
    public void setMaxFoodEaten(int maxFoodEaten) {
        this.maxFoodEaten = maxFoodEaten;
    }
    public int getMaxSleepTime() {
        return maxSleepTime;
    }
    public void setMaxSleepTime(int maxSleepTime) {
        this.maxSleepTime = maxSleepTime;
    }
    /**
     * Method to simulate life of animal
     * A decision what action to take depending on the values ​​of the parameters of the animal
     * @param food    amount food in cage
     * @param animals list animals in cage
     * @return amount food that is left in a cage
     */
    abstract public int animate(int food, List<Animal> animals);
    /**
     * Method describes process of eating food
     * @param food animal food
     * @return amount food that is left in a cage after meal
     */
    protected int eat(int food) {
        int eatenFood = new Random().nextInt(maxFoodEaten);
        if (food >= eatenFood) {
            satietyLevel += eatenFood;
            vitality += eatenFood;
            food -= eatenFood;
        } else {
            satietyLevel += food;
            vitality += food;
            food = 0;
        }
        logger.info(getClass().getSimpleName() + " " + alias + " eating");
        return food;
    }
    /**
     * Method describes animal sleep process
     */
    protected void sleep() {
        int sleepTime = new Random().nextInt(maxSleepTime);
        vivacityLevel += sleepTime;
        vitality += sleepTime;
        if (vivacityLevel > MAX_PERCENT) vivacityLevel = MAX_PERCENT;
        if (vitality > MAX_PERCENT) vitality = MAX_PERCENT;
        logger.info(getClass().getSimpleName() + " " + alias + " sleeping");
    }
    /**
     * Method describes animal move process
     */
    protected void move() {
        vivacityLevel -= minSleepTime + new Random().nextInt(minSleepTime);
        satietyLevel -= minFoodEaten + new Random().nextInt(minFoodEaten);
        vitality -= (new Random().nextInt(minSleepTime) + new Random().nextInt(minFoodEaten));
        if (vivacityLevel < MIN_PERCENT) vivacityLevel = MIN_PERCENT;
        if (satietyLevel < MIN_PERCENT) satietyLevel = MIN_PERCENT;
        if (vitality <= MIN_PERCENT) alive = false;
        logger.info(getClass().getSimpleName() + " " + alias + " moving");
    }
    @Override
    public String toString() {
        return alias + "[" + getClass().getSimpleName() + "]" + " ";
    }
}
