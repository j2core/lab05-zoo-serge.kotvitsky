package com.j2core.ice.week04.zooemulation;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.util.List;
import java.util.Random;
/**
 * Created by Serge Kotvitsky on 8/20/16.
 */
public class Worker {

    private String name;
    Logger logger = LogManager.getLogger(Worker.class);

    public Worker(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    /**
     * Method describes process of feeding animals worker zoo.
     * @param zoo List cells in zoo
     */
    public void feeding(List<Cage> zoo) {

        if (zoo == null) {
            logger.info("I don't feed homeless animals");
            return;
        }
        for (Cage cage : zoo) {
            if (cage.getAnimals().isEmpty()) {
                logger.info("In the cell, there is no animal");
                return;
            }
            switch (new Random().nextInt(5) + 1) {
                case 1: cage.setFoodLevel(0); break;
                case 2: cage.setFoodLevel(25); break;
                case 3: cage.setFoodLevel(50); break;
                case 4: cage.setFoodLevel(75); break;
                case 5: cage.setFoodLevel(100); break;
                default: break;
            }
            logger.info("In CAGE-" + cage.getId() + " added food " + cage.getFoodLevel());
        }
    }
}

