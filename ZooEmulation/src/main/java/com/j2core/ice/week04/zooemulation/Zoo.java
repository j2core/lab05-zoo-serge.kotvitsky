package com.j2core.ice.week04.zooemulation;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
/**
 * Created by Serge Kotvitsky on 8/20/16.
 */
public class Zoo {

    private List<Cage> zoo;
    Logger logger = LogManager.getLogger(Zoo.class);
    public Zoo(List<Cage> zoo) {
        this.zoo = zoo;
    }
    public List<Cage> getZoo() {
        return zoo;
    }
    public void setZoo(List<Cage> zoo) {
        this.zoo = zoo;
    }
    /**
     * Emulation works zoo
     * @param worker zoo worker
     */
    public void workingZoo(Worker worker) {

        if (worker == null) {
            logger.info("No employee of zoo");
            return;
        }
        if (zoo == null) {
            logger.info("Cells do not contain animals");
            return;
        }
        if (zoo.isEmpty()) {
            logger.info("Zoo Empty");
            return;
        }
        for (int day = 1; day < 8; day++) {
            logger.info("Day" + day);
            logger.info("__________");
            logger.info("Animals in cage");
            logger.info("------------------------------------------------------------------------------");
            for (Cage cage : zoo) {
                logger.info(cage.toString());
                logger.info("------------------------------------------------------------------------------");
            }
            logger.info("------------------------------------------------------------------------------");
            logger.info("It's time to feed the animals");
            worker.feeding(zoo);
            logger.info("Thank you. Now we observe the animals.");
            for (int time = 0; time < 24; time++) {
                if (time < 15) {
                    logger.info("------------------------------------------------------------------------------");
                    logger.info("Time   = " + (time + 9) + ":00 ");
                } else {
                    logger.info("------------------------------------------------------------------------------");
                    logger.info("Time  =  " + (time - 15) + ":00 ");
                }
                logger.info("------------------------------------------------------------------------------");
                for (Cage cage : zoo) {
                    cage.animateCage();
                }
            }
        }
    }
}