package com.j2core.ice.week04.zooemulation.animals;
import java.util.List;
import java.util.Random;
/**
 * Created by Serge Kotvitsky on 8/20/16.
 */
public class Hare extends Animal {

    public Hare(String alias, AnimalType animalType, int dangerLevel, int satietyLevel, int vivacityLevel,
                int vitality, int minFoodEaten, int minSleepTime) {
        super(alias, animalType, dangerLevel, satietyLevel, vivacityLevel, vitality, minFoodEaten, minSleepTime);
    }
    @Override
    public int animate(int food, List<Animal> animals) {

        if (getVivacityLevel() < new Random().nextInt(10) + 20)
            sleep();
        else if (getSatietyLevel() < new Random().nextInt(10) + 50)
            food = eat(food);
        else move();
        return food;
    }
}


