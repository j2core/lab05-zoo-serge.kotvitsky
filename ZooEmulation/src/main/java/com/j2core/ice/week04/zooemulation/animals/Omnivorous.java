package com.j2core.ice.week04.zooemulation.animals;
import java.util.List;
/**
 * Created by Serge Kotvitsky on 9/27/16.
 */
public interface Omnivorous {
    /**
     * Мethod describes the process of cannibalism
     * @param animals  List of victims
     */
    void eatingAnimal (List <Animal> animals);
}
