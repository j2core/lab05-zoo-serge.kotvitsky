package com.j2core.ice.week04.zooemulation.animals;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.util.List;
import java.util.Random;
/**
 * Created by Serge Kotvitsky on 8/20/16.
 */
public class Bear extends Animal implements Omnivorous {

    Logger logger = LogManager.getLogger(Bear.class);

    public Bear(String alias, AnimalType animalType, int dangerLevel, int satietyLevel, int vivacityLevel, int vitality,
                int minFoodEaten, int minSleepTime) {
        super(alias, animalType, dangerLevel, satietyLevel, vivacityLevel, vitality, minFoodEaten, minSleepTime);
    }
    @Override
    public int animate(int food, List<Animal> animals) {

        if (getVivacityLevel() < new Random().nextInt(15) + 30)
            sleep();
        else if (getSatietyLevel() < new Random().nextInt(15) + 40) {
            food = eat(food);
            if (getSatietyLevel() < new Random().nextInt(10) + 15)
                eatingAnimal(animals);
        } else move();
        return food;
    }
    @Override
    public void eatingAnimal(List<Animal> animals) {

        for (Animal animal : animals) {
            if (animal.isAlive() && getDangerLevel() > animal.getDangerLevel()
                    && !animal.getClass().getSimpleName().equals(getClass().getSimpleName())) {
                setSatietyLevel(new Random().nextInt(40) + 60);
                logger.info(" " + getClass().getSimpleName() + " " + getAlias() + " " + "eaten"
                        + " " + animal.getClass().getSimpleName() + " " + animal.getAlias());
                animal.setAlive(false);
                break;
            }
        }
    }
}



