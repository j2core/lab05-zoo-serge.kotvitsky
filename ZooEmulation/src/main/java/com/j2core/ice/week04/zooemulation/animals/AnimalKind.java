package com.j2core.ice.week04.zooemulation.animals;
/**
 * Created by Serge Kotvitsky on 8/26/16.
 */
public enum AnimalKind {
    SQUIRREL, BEAR, HARE, WOLF
}
