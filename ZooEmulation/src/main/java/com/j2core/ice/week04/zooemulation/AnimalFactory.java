package com.j2core.ice.week04.zooemulation;
import com.j2core.ice.week04.zooemulation.animals.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/**
 * Created by Serge Kotvitsky on 8/24/16.
 */
public class AnimalFactory {
    /**
     * Create animals according to their kind.
     * @param animalKinds - List AnimalKind
     * @return - an animal created.
     */
    public List<Animal> getAnimal(List<AnimalKind> animalKinds) {
        Random random = new Random();
        List<Animal> animals = new ArrayList<>();
        for (AnimalKind animalKind : animalKinds) {
            if (animalKind != null) {
                switch (animalKind) {
                    case SQUIRREL:
                        animals.add(new Squirrel(getAnimalAlias(), AnimalType.HERBIVOROUS, random.nextInt(2) + 1,
                                100, 100, 100, random.nextInt(5) + 1, random.nextInt(4) + 4)); break;
                    case BEAR:
                        animals.add(new Bear(getAnimalAlias(), AnimalType.OMNIVOROUS, random.nextInt(3) + 8,
                                100, 100, 100, random.nextInt(4) + 10, random.nextInt(4) + 6)); break;
                    case WOLF:
                        animals.add(new Wolf(getAnimalAlias(), AnimalType.PREDATOR, random.nextInt(3) + 5,
                                100, 100, 100, random.nextInt(5) + 7, random.nextInt(5) + 4)); break;
                    case HARE:
                        animals.add(new Hare(getAnimalAlias(), AnimalType.HERBIVOROUS, random.nextInt(2) + 1,
                                100, 100, 100, random.nextInt(5) + 1, random.nextInt(4) + 4)); break;
                    default:
                        throw new UnsupportedOperationException("Unknown AnimalKind " + animalKind +" for AnimalFactory");
                }
            }
        }
        return animals;
    }
    /**
     * Random selection of a name for the animal from the list
     * @return animal alias
     */
    private String getAnimalAlias() {
        String[] alias = new String[]{
                "Pashtet", "Kasper", "Sonya",
                "Tishka", "Nafanya", "Feliks",
                "Keks", "Kuzya", "Bonya",
                "Garfield", "Tomas", "Saimon",
                "Maks", "Loki", "Lucky",
                "Lexus", "Timon", "Almaz",
                "Zevs", "Leon", "Afonya",
                "Barsik", "Batman", "Eva",
                "Bagira"
        };
        return alias[new Random().nextInt(alias.length)];
    }
}
