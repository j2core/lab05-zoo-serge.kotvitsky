package com.j2core.ice.week04.zooemulation;
import com.j2core.ice.week04.zooemulation.animals.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.util.Iterator;
import java.util.List;
/**
 * Created by Serge Kotvitsky on 8/20/16.
 */
public class Cage {

    private int id;
    private List<Animal> animals;
    private int foodLevel;
    Logger logger = LogManager.getLogger(Cage.class);

    public Cage(int id) {
        this.id = id;    }
    public Cage(int foodLevel, int id) {
        this.foodLevel = foodLevel;
        this.id = id;
    }
    public Cage(List<Animal> animals, int id) {
        this.animals = animals;
        this.id = id;
    }
    public Cage(List<Animal> animals, int foodLevel, int id) {
        this.animals = animals;
        this.foodLevel = foodLevel;
        this.id = id;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public List<Animal> getAnimals() {
        return animals;
    }
    public void setAnimals(List<Animal> animals) {
        this.animals = animals;
    }
    public int getFoodLevel() {
        return foodLevel;
    }
    public void setFoodLevel(int foodLevel) {
        this.foodLevel = foodLevel;
    }
    /**
     * The method iterates through all the animals in a cage
     * and asks them information what they do at present,
     * and also removes non-existent or dead animals
     */
    public void animateCage() {
        if (animals != null) {
            for (Iterator<Animal> iter = animals.iterator(); iter.hasNext(); ) {
                Animal animal = iter.next();
                if (animal != null && animal.isAlive())
                    foodLevel = animal.animate(foodLevel, animals);
                else if (animal != null && !animal.isAlive()) {
                    logger.info(" " + animal.getClass().getSimpleName() + " " + animal.getAlias() + " kirdyk !!!");
                    iter.remove();
                } else iter.remove();
            }
        }
    }
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("CAGE-" + id + ":  ");
        if (animals != null) {
            for (Animal animal : animals) {
                if (animal == null)
                    str.append("null ");
                else
                    str.append(animal.toString());
            }
        }
        return str.toString();
    }
}
