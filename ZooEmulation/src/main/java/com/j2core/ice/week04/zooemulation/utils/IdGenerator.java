package com.j2core.ice.week04.zooemulation.utils;
import java.util.UUID;
/**
 * Created by Serge Kotvitsky on 9/20/16.
 */
public class IdGenerator {
    /**
     * Generator of unique id
     * @return unique id
     */
    public int generateUniqueId() {
        UUID id = UUID.randomUUID();
        String str = "" + id;
        int uid = str.hashCode();
        String filterStr = "" + uid;
        str = filterStr.replaceAll("-", "");
        return Integer.parseInt(str);
    }
}
