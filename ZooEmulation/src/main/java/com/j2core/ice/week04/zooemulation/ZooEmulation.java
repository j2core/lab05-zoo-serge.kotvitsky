package com.j2core.ice.week04.zooemulation;
import static com.j2core.ice.week04.zooemulation.animals.AnimalKind.*;
import com.j2core.ice.week04.zooemulation.animals.Animal;
import com.j2core.ice.week04.zooemulation.utils.IdGenerator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * Created by Serge Kotvitsky on 8/24/16.
 */
public class ZooEmulation {

    public static void main(String[] args) {

        AnimalFactory animalFactory = new AnimalFactory();
        IdGenerator id = new IdGenerator();

        List<Animal> animals1 = animalFactory.getAnimal(new ArrayList<>(Arrays.asList(BEAR,BEAR,BEAR,WOLF,WOLF,WOLF,WOLF,
                WOLF,HARE,HARE,HARE,HARE,HARE,SQUIRREL,SQUIRREL,SQUIRREL,SQUIRREL,SQUIRREL,SQUIRREL,SQUIRREL,SQUIRREL,
                HARE,HARE,HARE,HARE,HARE,SQUIRREL,SQUIRREL,SQUIRREL,SQUIRREL,WOLF,WOLF,WOLF,WOLF,WOLF)));

        List<Animal> animals2 = animalFactory.getAnimal(new ArrayList<>(Arrays.asList(BEAR,BEAR,BEAR,WOLF,WOLF,WOLF,WOLF,
                WOLF,HARE,HARE,HARE,HARE,HARE,SQUIRREL,SQUIRREL,SQUIRREL,SQUIRREL,SQUIRREL,HARE,HARE,HARE,SQUIRREL,
                SQUIRREL,HARE,SQUIRREL,SQUIRREL,HARE,HARE,HARE,WOLF,WOLF,WOLF,WOLF,WOLF)));

        Zoo zoo = new Zoo(new ArrayList<>(Arrays.asList(new Cage(animals1,100,id.generateUniqueId()),
                new Cage(animals2,100,id.generateUniqueId()))));
        zoo.workingZoo(new Worker("Ivan"));
    }
}