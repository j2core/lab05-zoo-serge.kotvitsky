package com.j2core.ice.week04.zooemulation.animals;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.util.List;
import java.util.Random;
/**
 * Created by Serge Kotvitsky on 8/20/16.
 */
public class Wolf extends Animal implements Predators {

    private Logger logger = LogManager.getLogger(Wolf.class);

    public Wolf(String alias, AnimalType animalType, int dangerLevel, int satietyLevel, int vivacityLevel,
                int vitality, int minFoodEaten, int minSleepTime) {
        super(alias, animalType, dangerLevel, satietyLevel, vivacityLevel, vitality, minFoodEaten, minSleepTime);
    }
    @Override
    public int animate(int food, List<Animal> animals) {

        if (getVivacityLevel() < new Random().nextInt(10) + 40)
            sleep();
        else if (getSatietyLevel() < new Random().nextInt(20) + 30) {
            food = eat(food);
            if (getSatietyLevel() < new Random().nextInt(10) + 10)
                eatingHerbivorousAnimal(animals);
        } else move();
        return food;
    }
    @Override
    public void eatingHerbivorousAnimal(List<Animal> animals) {
        for (Animal animal : animals) {
            if (animal.isAlive() && animal.getAnimalType() == AnimalType.HERBIVOROUS
                    && getDangerLevel() > animal.getDangerLevel()) {
                setSatietyLevel(new Random().nextInt(50) + 50);
                logger.info(" " + getClass().getSimpleName() + " " + getAlias() + " " + "eaten"
                        + " " + animal.getClass().getSimpleName() + " " + animal.getAlias());
                animal.setAlive(false);
                break;
            }
        }
    }
}


